import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { IonicApp, IonicModule } from 'ionic-angular';

import { PhotoLibraryService } from './photo-library.service';
import { PhotoAppRootComponent } from './photo-app-root/photo-app-root.component';
import { PhotosListPageComponent } from './photos-list-page/photos-list-page.component';
import { PhotoDetailsPageComponent } from './photo-details-page/photo-details-page.component';
import { PhotoListItemComponent } from './photo-list-item/photo-list-item.component';
import { PhotoDetailsEditorComponent } from './photo-details-editor-page/photo-details-editor-page.component';

@NgModule({
  declarations: [
    PhotoAppRootComponent,
    PhotosListPageComponent,
    PhotoDetailsPageComponent,
    PhotoListItemComponent,
    PhotoDetailsEditorComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(PhotoAppRootComponent)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    PhotoAppRootComponent,
    PhotosListPageComponent,
    PhotoDetailsPageComponent,
    PhotoDetailsEditorComponent
  ],
  providers: [PhotoLibraryService]
})
export class AppModule {}
