import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {NgForm} from "@angular/forms";

import { Photo } from '../entities/photo';
import { PhotoLibraryService } from '../photo-library.service';

@Component({
  selector: 'photo-details-editor-page',
  templateUrl: 'photo-details-editor-page.component.html'
})
export class PhotoDetailsEditorComponent {
  original: Photo;
  photo: Photo;
  @ViewChild(NgForm) form: NgForm;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private backend: PhotoLibraryService)
  {
  }

  ionViewDidLoad() {
    this.original = this.photo = this.navParams.get('photo');
    this.reset();
  }

  back() {
     this.navCtrl.pop();
  }

  save() {
    this.backend.updatePhoto(this.photo).then(() => this.back());
  }

  reset() {
    this.photo = Object.assign({}, this.original);
  }
}
