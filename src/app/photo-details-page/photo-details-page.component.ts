import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Photo } from '../entities/photo';
import { PhotoLibraryService } from '../photo-library.service';
import { PhotoDetailsEditorComponent } from '../photo-details-editor-page/photo-details-editor-page.component';

@Component({
  selector: 'photo-details-page',
  templateUrl: 'photo-details-page.component.html'
})
export class PhotoDetailsPageComponent {
  photo: Photo = null;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private backend: PhotoLibraryService) {
  }

  ionViewWillEnter() {
    var photoId = this.navParams.get('photo').id;
    this.backend.getPhoto(photoId).then(photo => {
      this.photo = photo;
    });
  }

  back() {
     this.navCtrl.pop();
  }

  deletePhoto() {
     this.backend.deletePhoto(this.photo.id).then(
       () => this.back()
     );
   }

   toggleStar() {
     this.photo.starred = !this.photo.starred;
     this.backend.updatePhoto(this.photo);
   }

   editPhoto() {
     this.navCtrl.push(PhotoDetailsEditorComponent, { photo: this.photo });
   }
 }
